Crazyflie Arduino Controller Demo

This is a simple Arduino sketch to demonstrate the capability to use an Arduino connected to a pair of joysticks and a nRF24L01+ breakout board to fly a CrazyFlie.

main file: CrazyFlie_Transmitter


BOM:
Name                        Source	   PN	       Qty  Cost Ea	Comment
nRF24L001+ Breakout Board   Sparkfun   WRL-00705   1   19.95	
Joystick, Thumb	            Sparkfun   COM-09032   2    3.95	
Joystick Breakout Board     Sparkfun   BOB-09110   2    1.95	
Arduino Pro Mini 5v 16MHz   Sparkfun   DEV-11113   1    9.95	
Serial LCD                  Adafruit   782         1   24.95	
Antenna, 2.4 GHz Dipole     Adafruit   944         1    6.95	
Antenna, 2.4 GHz 5dBi Gain  Adafruit   945         -    8.95	Longer Range Option
Male Headers, strip	        Adafruit   392         100  7.95	10 Strips of 36
Jumpers, Female to Female   Sparkfun   PRT-08430   3    3.95	pack of 10
Battery, Li Ion	            Adafruit   258         1   22.95	Alternate - Use 3 AA
Charger/Booster, LiPo       Sparkfun   PRT-11231   1   19.95	Convert battery to 5V
Switch, SPST                various                1
Switch, pushbutton N.O.     various                4
Proto-board for Power Bus   Adafruit   589         1    8.95	Pack of 3, only need 1



Wiring:
See code listing
